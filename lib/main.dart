import 'package:flutter/material.dart';
import 'package:dubp/dubp.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _message;
  String _mnemonic;
  String _dewif;
  String _pin;
  String _publicKey;
  String _sig;

  @override
  void initState() {
    super.initState();
    DubpRust.setup();
    _message = "toto";
    _mnemonic = "";
    _dewif = "";
    _pin = "";
    _publicKey = "";
    _sig = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Message: toto',
            ),
            Text(
              'Mnemonic: ',
            ),
            Text(
              _mnemonic,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            Text(
              'Dewif: ',
            ),
            Text(
              _dewif,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            Text(
              'Pin: ',
            ),
            Text(
              _pin,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            Text(
              'PublicKey: ',
            ),
            Text(
              _publicKey,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            Text(
              'Signature: ',
            ),
            Text(
              _sig,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            const SizedBox(height: 100),
            RaisedButton(
              color: Colors.greenAccent,
              child: Text(
                'Gen wallet',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: _genNewMnemonicAndSign,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _changePin,
        tooltip: 'ChangePin',
        child: Icon(Icons.refresh),
      ),
    );
  }

  void _changePin() async {
    NewWallet _newWallet = await DubpRust.changeDewifPin(
        dewif: _dewif, oldPin: _pin, newPinLength: PinLength.eight);
    setState(() {
      _dewif = _newWallet.dewif;
      _pin = _newWallet.pin;
      _publicKey = _newWallet.publicKey;
    });
  }

  void _genNewMnemonicAndSign() async {
    _mnemonic = await DubpRust.genMnemonic(language: Language.french);
    NewWallet _newWallet = await DubpRust.genWalletFromMnemonic(
        mnemonic: _mnemonic, language: Language.french);
    String _sig = await DubpRust.sign(
        dewif: _newWallet.dewif, pin: _newWallet.pin, message: _message);
    setState(() {
      this._mnemonic = _mnemonic;
      _dewif = _newWallet.dewif;
      _pin = _newWallet.pin;
      _publicKey = _newWallet.publicKey;
      this._sig = _sig;
    });
  }
}
