# dubp-rs-flutter

Flutter + Rust = :heart:

Rust binding of [dubp-rs-libs] for flutter. Forked from template [flutterust].

## How to use it

See [dart plugin `dubp_rs` README](packages/dubp_rs/README.md).

## How it works?

The simple idea here is that we build our rust code for all supported targets
then build a Flutter Package that uses these targets.

For more details, see [flutterust] README.

[dubp-rs-libs]: https://git.duniter.org/libs/dubp-rs-libs
[flutterust]: https://github.com/shekohex/flutterust

## Project Structure

```tree
.
├── android
├── ios
├── lib                     <- The Flutter App Code
├── native                  <- Containes all the Rust Code
│   └── dubp-rs
├── packages                <- Containes all the Dart Packages that bind to the Rust Code
│   └── dubp_rs
├── target                  <- The compiled rust code for every arch
│   ├── aarch64-apple-ios
│   ├── aarch64-linux-android
│   ├── armv7-linux-androideabi
│   ├── i686-linux-android
│   ├── x86_64-apple-ios
│   └── x86_64-linux-android
```

## Contribute

### Prerequisites

* Android SDK
* Android NDK
* Rust and cargo
* Cargo plugin cargo-make: `cargo install cargo-make`
* LLVM/Clang (see dedicated section below)

You will also need to add targets for all Android architectures:

```sh
rustup target add aarch64-linux-android armv7-linux-androideabi x86_64-linux-android i686-linux-android
```

If you develop on mac, you can also add targets for iOS:

```sh
rustup target add aarch64-apple-ios x86_64-apple-ios
```

### LLVM/Clang

The project use [`dart-bindgen`](https://github.com/sunshine-protocol/dart-bindgen) which requires LLVM/Clang. Install LLVM (10+) in the following way:

#### ubuntu/linux

1. Install libclangdev - `sudo apt-get install libclang-dev`.

#### Windows

1. Install Visual Studio with C++ development support.
2. Install [LLVM](https://releases.llvm.org/download.html) or `winget install -e --id LLVM.LLVM`.

#### MacOS

1. Install Xcode.
2. Install LLVM - `brew install llvm`.

## Build and Run

### Compile Rust

#### For development

**To reduce the compilation time of the Rust code** during your development, you can **compile only for the target corresponding to your android emulator**. Here is how to do it **depending on the architecture of your emulator**:

* 32bit emulator (`x86`/`i686` architecture)

```sh
cargo make android-dev32
```

* 64bit emulator (`x86_64` architecture)

```sh
cargo make android-dev
```

#### For release

In the Root of the project simply run:

```sh
cargo make
```

WARNING: This will take a lot of time because the Rust code will have to be recompiled for each different architecture, 4 times for android and 2 times for iOS!

### Create emulator

To test the project, you need a smartphone connected to your development computer. If you don't want to use a real physical smartphone, you can create an emulator:

```sh
flutter emulators --create
```

### Run flutter app

Then run flutter app normally:

```sh
flutter run
```
